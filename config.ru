require './awesome_journeys.rb'

if ENV['RACK_ENV'] == 'production'
  use Rack::Session::Dalli, :cache => $memcache_dalli_client
else
  use Rack::Session::Pool, :expire_after => 2592000
end

map('/search') do
  run AwesomeJourneys::SearchController
end

map('/itinerary') do
  run AwesomeJourneys::ItineraryController
end

map('/stay') do
  run AwesomeJourneys::StayController
end

map('/aut') do
  run AwesomeJourneys::UserController
end

map('/') do
  run AwesomeJourneys::AwesomeJourneysController
end

