require 'sinatra/base'
require 'active_support/core_ext/numeric/time.rb'
require 'active_support/core_ext/date/calculations.rb'
require 'active_support/core_ext/module/delegation.rb'
require './database/config.rb'

require './settings.rb'
require './app/models/model.rb'
Dir["./app/helpers/*.rb"].each { |file| require file }
require './app/controllers/application_controller.rb'
Dir["./app/controllers/*.rb"].each { |file| require file }

if ENV['RACK_ENV'] == 'production'
  require 'dalli'
  require 'rack/session/dalli' # For Rack sessions in Dalli
  $memcache_dalli_client = Dalli::Client.new(ENV['MEMCACHE_SERVERS'], 
                              :username => ENV['MEMCACHE_USERNAME'],
                              :password => ENV['MEMCACHE_PASSWORD'],
                              :expires_in => 300)
end