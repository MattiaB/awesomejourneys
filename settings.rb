module AwesomeJourneys
  class Sinatra::Base
    configure do
      set :views, ['views/layouts', 'views/pages', 'views/partials']
      set :public_folder, './public'
      set :static_cache_control, [:public,:max_age => 2592000]
    end
  end
end