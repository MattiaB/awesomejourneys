class Numeric
  def to_days
    self / 60 / 60 / 24
  end
end
