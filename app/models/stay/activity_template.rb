module AwesomeJourneys
  class ActivityTemplate
    include DataMapper::Resource
    property :id,           Serial
    property :name,         String, :length => 60
    property :description,  Text
    property :duration,     Integer
    property :cost,         Decimal, :scale => 2, :precision => 5

    belongs_to  :location,    :required => false
    has n,      :activities,  :required => false
    def cost_to_s
        cost.to_s('2F')
    end

    def activity_factory
      Activity.create(:activity_template => self)
    end
  end
end