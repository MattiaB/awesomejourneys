module AwesomeJourneys
  class OptionValue
    include DataMapper::Resource
    property :value,  Object
    
    belongs_to :option,           :key => true
    belongs_to :stay_template,    :key => true
  end
end