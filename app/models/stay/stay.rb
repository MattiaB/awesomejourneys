module AwesomeJourneys
  class Stay
    include DataMapper::Resource
    delegate :name,:description,:duration,:start_location,:end_location, :to => :stay_template, :allow_nil => true
    property :id,         Serial
    property :offset_number,      Integer

    belongs_to :stay_template
    has n, :activities, :order => [ :offset_number.asc ]
    def add_activity_by_activity_template_id(activity_template_id)
      activity_template = ActivityTemplate.first(:id => activity_template_id)
      raise "Impossibile aggiungere attivita' per la location corrente" unless activity_template.location.name == self.start_location.name 
      activities << activity_template.activity_factory unless activity_template.nil?
    end

    def delete_activity_by_id(activity_id)
      activities.get(activity_id).destroy!
    end

    def delete
      self.activities.destroy!
      self.destroy!
    end

    def cost_to_s
      local_cost = cost
      if local_cost
        BigDecimal.new(local_cost.to_s).to_s('2F')
      else
        ''
      end
    end

    def cost
      activities_cost = activities.reduce(0.0) { |total_cost, activity| total_cost + activity.cost  }
      stay_template.cost + activities_cost
    end
  end
end