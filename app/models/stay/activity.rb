module AwesomeJourneys
  class Activity
    include DataMapper::Resource
    delegate :name,:description,:duration,:cost, :cost_to_s,:location, :to => :activity_template, :allow_nil => true
    property :id,       Serial
    property :offset_number,      Integer

    belongs_to :activity_template
    belongs_to :stay_template,    :required => false
    def deep_clone
      activity_template.activity_factory
    end
  end
end