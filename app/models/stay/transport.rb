module AwesomeJourneys
  class Transport < StayTemplateLeaf
    property :transport_type,   Enum[ :airplane, :bus, :car, :train, :public_transport, :by_foot, :ferry ]
    property :transport_cost,   Decimal, :scale => 2, :precision => 5
    def cost
      super + transport_cost
    end
  end
end