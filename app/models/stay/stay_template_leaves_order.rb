module AwesomeJourneys
  class StayTemplateLeavesOrder
    include DataMapper::Resource
    property :offset_number,      Integer
    
    belongs_to :source, 'StayTemplate',     :key => true
    belongs_to :target, 'StayTemplateLeaf', :key => true
  end
end