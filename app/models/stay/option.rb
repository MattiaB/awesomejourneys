module AwesomeJourneys
  class Option
    include DataMapper::Resource
    property :id,       Serial
    property :name,     String, :length => 60
    
    has n, :stay_templates, :through => :option_values
    has n, :option_values
  end
end