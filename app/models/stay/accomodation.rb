module AwesomeJourneys
  class Accomodation < StayTemplateLeaf
    property :daily_cost,     Decimal, :scale => 2, :precision => 5
    property :classification, Enum[ :one, :two, :three, :four, :five ]
    def cost
      super + (daily_cost * duration.to_days)
    end
  end
end