module AwesomeJourneys
  class StayTemplate
    include DataMapper::Resource
    property :id,           Serial
    property :type,         Discriminator
    property :name,         String, :length => 70
    property :description,  Text
    property :duration,     Integer

    belongs_to :start_location,   :model => Location
    belongs_to :end_location,     :model => Location

    has n, :activities, :order => [ :offset_number.asc ]
    has n, :options,  :through => :option_values
    has n, :option_values

    has n, :stay_template_leaves_order, :child_key => [ :source_id ]
    has n, :stay_template_leaves, self, :through => :stay_template_leaves_order, :via => :target
    def add_stay_template_leaf(leaf)
      if leaf.kind_of? StayTemplateLeaf
        stay_template_leaves << leaf
      end
    end

    def add_activity(activity)
      if activity.location == start_location || activity.location == end_location
        activities << activity
      return true
      end
      false
    end

    def cost
      activities.reduce(0.0) { |total_cost, activity| total_cost + activity.cost  }
    end

    def cost_to_s
      local_cost = cost
      if local_cost
        BigDecimal.new(local_cost.to_s).to_s('2F')
      else
        ''
      end
    end

    def instantiate(user)
      stay = Stay.new
      stay.stay_template = self
      self.activities.each do |activity|
        stay.activities << activity.deep_clone
      end
      stay.save
      stay
    end
  end
end