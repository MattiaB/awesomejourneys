require_relative 'stay_template.rb'
module AwesomeJourneys
  class StayTemplateComposite < StayTemplate
    def initialize()
      super()
      @sub_stay_template = []
    end

    def add_stay_template(stay_template)
      @sub_stay_template << stay_template
    end

    def remove_stay_template(stay_template)
      @sub_stay_template.delete(stay_template)
    end

    #interface methods
    def cost
      @sub_stay_template.inject(0.0){|total_cost, stay| total_cost + stay.cost }
    end

    def duration
      @sub_stay_template.inject(Time.new 0){|total_time, stay| total_time + stay.duration }
    end

    #otherwise
    def [](index)
      @sub_stay_template[index]
    end

    def []=(index, stay_template)
      @sub_stay_template[index] = stay_template
    end

    def <<(stay_template)
      @sub_stay_template << stay_template
    end
  end
end