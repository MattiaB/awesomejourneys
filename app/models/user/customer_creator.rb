module AwesomeJourneys
  class CustomerCreator
    include Creator
    
    def get_controller
      ManagementController.new self
    end
  end
end