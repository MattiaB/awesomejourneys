require_relative 'abstract_user.rb'
module AwesomeJourneys
  module Creator
    include AbstractUser
    def initialize(component)
      raise "Only User can be decoreted" unless component.kind_of? User
      @decorated = component
    end

    def user
      @decorated
    end

    #Chiama sul oggetto decorato i metodi che non trova nel Creator
    def itineraries
      @decorated.itineraries
    end

    def itineraries=(itineraries_param)
      @decorated.itineraries << itineraries_param
    end

    def journeys
      @decorated.journeys
    end

    def journeys=(journeys_param)
      @decorated.journeys << journeys_param
    end

    def name
      @decorated.name
    end

    def name=(name_param)
      @decorated.name = name_param
    end

    def add_itinerary(itinerary)
      itinerary.user = @decorated
    end

    def create_stay(stay_template)
      stay_template.instantiate(@decorated)
    end
  end
end