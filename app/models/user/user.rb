require 'bcrypt'

module AwesomeJourneys
  class User
    include AbstractUser
    include DataMapper::Resource
    property :id,           Serial
    property :name,         String, :length => 1..50, :unique => true
    property :passwordhash, String, :length => 60, :required => true
    property :salt,         String, :length => 29, :required => true
    property :type,         Discriminator

    has n, :itineraries
    has n, :journeys
    has n, :itinerary_or_journey_searches, 'SearchResultsItineraryOrJourney'
    has n, :stay_searches, 'SearchResultsStay'
    has n, :activity_searches, 'SearchResultsActivity'
    def self.create!(username, password)
      password_salt = BCrypt::Engine.generate_salt
      password_hash = BCrypt::Engine.hash_secret(password, password_salt)
      raise "length of the user name must be greater than zero" if username.length == 0
      user = self.first_or_create({:name => username}, {:salt => password_salt, :passwordhash => password_hash})
      user
    end

    def self.login(username,password)
      user = User.first(:name => username)
      if user && user.passwordhash == BCrypt::Engine.hash_secret(password, user.salt)
      return user
      end
      return nil
    end
  end
end