module AwesomeJourneys
  # Non lo usiamo.
  # Servirebbe in funzionalità in cui l'utente non registrato può comunque fare delle operazioni.
  # Tuttavia nella parte del nostro sistema che abbiamo implementato non è prevista l'interazione col sistema
  # se non si è loggati. Quindi non lo usiamo.
  class UserProxy
    include AbstractUser
  end
end