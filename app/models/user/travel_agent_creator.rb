require_relative 'creator.rb'
module AwesomeJourneys
  class TravelAgentCreator
    include Creator
    def initialize(component)
      super component
      raise "Only Travel Agent can become Travel Agent Creator" unless component.kind_of? TravelAgent
    end

    def get_controller
      ManagementControllerPro.new self
    end
  end
end