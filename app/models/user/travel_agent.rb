module AwesomeJourneys
  class TravelAgent < User
    def get_creator
      TravelAgentCreator.new self
    end
  end
end