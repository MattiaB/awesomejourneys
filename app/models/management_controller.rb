module AwesomeJourneys
  class ManagementController
    attr_accessor :itinerary
    attr_accessor :current_stay
    def initialize(creator)
      @creator = creator
    end

    #Start ManageItineraryBase
    def create_itinerary()
      unless @itinerary.nil?
      @itinerary.delete
      end
      @itinerary = Itinerary.new
      @creator.add_itinerary @itinerary
      @itinerary.save
      @itinerary
    end

    def set_basic_info(info)
      @itinerary.set_basic_info(info)
    end

    def select_stay(id)
      stay_template = StayTemplate.get(id)
      @current_stay = @creator.create_stay(stay_template)
      @current_stay
    end

    def set_stay_parameters(info)
      #Non ho capito che informazioni deve settare e in che formato gliele fornisce l'utente
    end

    def save_stay
      @itinerary.add_stay @current_stay
      @current_stay.save
      @itinerary.stays.length #Aggiorna le tappe nell'itinerario tenuto in memoria
      @itinerary
    end

    def save_itinerary()
      status = {}
      status[:status] = :partial
      if @itinerary.stays.count != 0
        status = @itinerary.stays.reduce({:status => :complete, :prev_location => @itinerary.stays[0].start_location}) do |before_location, stay|

          before_location[:status] = :partial if(before_location[:prev_location].name != stay.start_location.name)
          before_location[:prev_location] = stay.end_location
          before_location
        end
      end
      @itinerary.status = status[:status]

      @itinerary.save
      @itinerary = nil
    end

    def delete_stay(stay_id)
      @itinerary.delete_stay(stay_id)
    end

    def delete_itinerary(id = nil) # Se un itinerario è legato ad un journey non deve essere cancellato
      if id.nil?
      itinerary = @itinerary
      else
        itinerary = Itinerary.get(id)
      end
      return nil unless itinerary
      raise "The itinerary cannot be delete bacause has a journey associated" unless itinerary.journey.nil?
      itinerary.delete
    end

    def edit_itinerary(id)
      #restituisce un itineario per la modifica solo se richiesto dal suo creator
      @itinerary = Itinerary.first(:id => id, :user => @creator.user)
    end

    #End ManageItineraryBase
    def get_activities()
      @current_stay.activities
    end

    #ManageActivitiesInStay - dummy
    def add_activity(activity_template_id)
      @current_stay.add_activity_by_activity_template_id(activity_template_id)
    #oppure (non c'è la gestione dell'offset)
    #activity_template = ActivityTemplate.first(:id => activity_id)
    #activity = Activity.new(:activity_template => activity_template)
    #stay.activities << activity
    end

    def delete_activity(activity_id)
      #bisogna scorrere la lista di activity e controllare se l'id del template corrisponde a activity_idrac
      @current_stay.delete_activity_by_id(activity_id)

    end

    def get_itinerary_by_id(itinerary_id)
      Itinerary.get(itinerary_id)
    end

    def add_transfer_stay(stay_id)
      from_this_stay = @itinerary.stays.get(stay_id)
      next_offset = from_this_stay.offset_number + 1
      to_this_stay = @itinerary.stays.first(:offset_number => next_offset)
      raise "Cannot add a transfer stay if you don't have a destination stay" unless to_this_stay

      transfer_stay_template = Transport.first( :start_location =>from_this_stay.end_location,
      :end_location =>to_this_stay.start_location)
      raise "Doesn't exist the tranfer stay" unless transfer_stay_template
      transfer_stay = @creator.create_stay(transfer_stay_template)
      transfer_stay.offset_number = next_offset

      @itinerary.stays.map! do |stay|
        if (stay.offset_number >= next_offset)
        stay.offset_number += 1
        end
        stay
      end

      @itinerary.stays << transfer_stay
      @itinerary.stays.save
      @itinerary.stays.sort!{|a,b| a.offset_number <=> b.offset_number}
    end
  end
end
