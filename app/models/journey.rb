module AwesomeJourneys
  class Journey
    include DataMapper::Resource
    property :id,         Serial
    property :name,       String
    property :start,      DateTime
    property :end,        DateTime
    belongs_to :user
    belongs_to :itinerary
  end
end