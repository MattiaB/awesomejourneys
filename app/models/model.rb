require './app/models/search/search_results_itinerary_or_journey.rb'
Dir["./app/models/search/*.rb"].each { |file| require file }
require './app/models/itinerary.rb'
require './app/models/journey.rb'


require './app/models/stay/location.rb'
require './app/models/stay/option.rb'
require './app/models/stay/option_value.rb'
require './app/models/stay/stay_template_leaves_order.rb'
require './app/models/stay/stay_template.rb'
require './app/models/stay/stay_template_composite.rb'
require './app/models/stay/stay_template_leaf.rb'
require './app/models/stay/accomodation.rb'
require './app/models/stay/transport.rb'
require './app/models/stay/hand_made_stay.rb'

require './app/models/management_controller.rb'
require './app/models/management_controller_pro.rb'

require './app/models/user/abstract_user_component.rb'
require './app/models/user/abstract_user.rb'
require './app/models/user/creator.rb'
require './app/models/user/customer_creator.rb'
require './app/models/user/travel_agent_creator.rb'
require './app/models/user/user_proxy.rb'
require './app/models/user/user.rb'
require './app/models/user/customer.rb'
require './app/models/user/travel_agent.rb'

Dir["./app/models/stay/*.rb"].each { |file| require file }
DataMapper.finalize