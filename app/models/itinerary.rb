module AwesomeJourneys
  class Itinerary
    include DataMapper::Resource
    property :id,           Serial
    property :name,         String, :length => 60
    property :description,  Text
    property :category,     String
    property :status,       Enum[ :partial, :complete, :inconsistent ], :default => :inconsistent
    has n, :stays, :order => [ :offset_number.asc ]
    has 1, :journey
    belongs_to :user
    
    def add_stay(stay)
      if stay.offset_number.nil?
        last_stay = self.stays.last
        offset_number = 0
        if last_stay
        offset_number = last_stay.offset_number + 1
        end
      stay.offset_number = offset_number
      end
      self.stays << stay
    end

    def set_basic_info(info)
      [:name, :description, :category].each do |key|
        self.send((key.to_s+'=').to_sym, info[key])
      end
    end

    def delete_stay(stay_id)
      #stay_index = stays.index{|stay|stay.id==stay_id}
      #stay_in_array = stays[stay_index]
      stay = stays.get(stay_id)
      stays.delete_if do |stay_of_collection|
        stay.id == stay_of_collection.id
      end
      stay.delete
    end

    def delete
      self.stays.each do |stay|
        stay.delete
      end
      self.destroy!
    end
    def instantiate(user, params)
      raise "L'itinerario non e' completo" unless self.status == :complete
      journey_data = {:user => user, :itinerary => self}.merge(params) 
      Journey.create(journey_data)
    end
  end
end