module AwesomeJourneys
  class GraspSearchController
    def initialize(user)
      @user = user
    end
    def search_itinerary_or_journey(search)
      itineraries = Itinerary.all(:name.like => "%#{search}%") | Itinerary.all(:category.like => "%#{search}%")
      journeys = Journey.all(:name.like => "%#{search}%")
      search(SearchResultsItineraryOrJourney,itineraries,journeys)
    end

    def search_stay(search)
      stay_templates = StayTemplate.all(:name.like => "%#{search}%") | StayTemplate.all(:description.like => "%#{search}%")
      search(SearchResultsStay,stay_templates)
      #Dummy Search
      #StayTemplate.all
    end

    def search_activities # (search, cost)
      #ActivityTemplate.all(:name.like => "%#{search}%") | ActivityTemplate.all(:description.like => "%#{search}%") & ActivityTemplate.all(:cost => cost)
      #Dummy Search
      activities =  ActivityTemplate.all
      search(SearchResultsActivity,activities)
    end
    def search_transport_stay(start_location, end_location)
      Transport.all(:start_location => start_location, :end_location => end_location)
    end
    private
    def search(results_class, *args)
      results = results_class.new
      results.add_results(*args)
      results.user = @user
      results.save
      results
    end
  end
end