module AwesomeJourneys
  class SearchResultsStay
    include DataMapper::Resource
    property :id, Serial
    has n, :stay_templates,'StayTemplate', :through => Resource
    belongs_to :user
    def add_results(stay_templates)
      self.stay_templates = stay_templates
    end

    def each(&block)
      stay_templates.each(&block)
    end
  end
end