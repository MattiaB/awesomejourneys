module AwesomeJourneys
  class SearchResultsItineraryOrJourney
    include DataMapper::Resource
    property :id, Serial
    has n, :itineraries, :through => Resource
    has n, :journeys, :through => Resource
    belongs_to :user
    def add_results(itineraries,journeys)
      self.itineraries = itineraries
      self.journeys = journeys
    end
  end
end