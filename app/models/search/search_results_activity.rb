module AwesomeJourneys
  class SearchResultsActivity
    include DataMapper::Resource
    property :id, Serial
    has n, :activity_templates,'ActivityTemplate', :through => Resource
    belongs_to :user
    def add_results(activity_templates)
      self.activity_templates = activity_templates
    end

    def each(&block)
      activity_templates.each(&block)
    end
  end
end