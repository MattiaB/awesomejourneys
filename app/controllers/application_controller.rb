module AwesomeJourneys
  class ApplicationController < Sinatra::Base
    helpers Helpers
    before(/^(?!\/(signup|login|logout))/) do
      redirect '/aut/login' unless login?
    end
    get '/favicon.ico' do
      cache_for 7*24*60*60
      status 404
    end
  end
end
