module AwesomeJourneys
  class SearchController < ApplicationController
    before do
      user = session[:user]
      @search = GraspSearchController.new(user)
    end
    get '/itineraryorjourney' do
      erb "search/itinerary_or_journey".to_sym
    end
    post '/itineraryorjourney' do
      @results = @search.search_itinerary_or_journey(params[:search])
      erb "search/itinerary_or_journey".to_sym
    end

    get '/stays' do
      erb "search/stays".to_sym
    end

    post '/stays' do
      @results = @search.search_stay(params[:search])
      erb "search/stays".to_sym
    end

    get '/activities' do
      erb "search/activities".to_sym
    end

    post '/activities' do
      @results = @search.search_activities()
      erb "search/activities".to_sym
    end

  end
end