module AwesomeJourneys
  class UserController < ApplicationController
    get "/login" do
      redirect "/" if session[:user] # L'utente è già loggato
      erb :login
    end

    get "/signup" do
      redirect "/" if session[:user] # L'utente è già loggato
      erb :signup
    end

    post "/signup" do
      redirect "/" if session[:user] # L'utente è già loggato e quindi registrato
      
      session[:user] = Customer.create!(params[:username],params[:password])
      redirect "/"
    end

    post "/login" do
      redirect "/" if session[:user] # L'utente è già loggato
      user = User.login(params[:username],params[:password])
      if user
          session[:user] = user
          redirect "/"
        end
        
      # Errore nel login
      session[:error] = "Can't login with the given data. Try again." # Flash message
      redirect "/aut/login"
    end

    get "/logout" do
      session[:user] = nil
      session[:controller] = nil
      redirect "/"
    end

  end
end
