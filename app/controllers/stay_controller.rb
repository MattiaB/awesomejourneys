module AwesomeJourneys
  class StayController < ApplicationController
    
    before do
      @creator = session[:user].get_creator
      @controller = session[:controller] 
      @controller = session[:controller] = @creator.get_controller unless @controller
    end

    get '/edit' do
      @stay = @controller.current_stay
      erb 'stay/show'.to_sym
    end
    
    post '/edit' do
    end
    
    get '/select/:id' do
      @controller.select_stay(params[:id])
      redirect '/stay/edit'
    end
    
    get '/save' do
      @controller.save_stay
      redirect '/itinerary'
    end
    
    get '/delete/:id' do
      @controller.delete_stay params[:id]
      redirect '/itinerary'
    end
    
    get '/addtranfert/:id' do
      @controller.add_transfer_stay params[:id]
      redirect '/itinerary'
    end
    
    get '/manageactivity' do
      @activities = GraspSearchController.new(session[:user]).search_activities
      erb 'stay/manage_activity'.to_sym
    end
    
    get '/addactivity/:id' do
      @controller.add_activity params[:id]
      redirect "/stay/edit"
    end
  end
end