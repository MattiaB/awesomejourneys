module AwesomeJourneys
  class AwesomeJourneysController < ApplicationController
    get "/" do
      @user = User.first(:id => session[:user].id)
      erb :index
    end
  end
end