module AwesomeJourneys
  class ItineraryController < ApplicationController
    
    before do
      @creator = session[:user].get_creator
      @controller =   session[:controller] 
      @controller =   session[:controller] = @creator.get_controller unless @controller
    end
    
    get '/' do
      @itinerary = @controller.itinerary
      erb 'itinerary/show'.to_sym
    end
    
    get '/edit/:id' do
      @controller.edit_itinerary(params[:id])
      redirect '/itinerary'
    end
    
    get '/delete/:id' do
      @controller.delete_itinerary(params[:id])
      redirect '/'
    end 
    
    get '/create' do
     @controller.create_itinerary
     redirect '/itinerary/providebasicinfo'
    end
    
    get '/providebasicinfo' do
      @itinerary = @controller.itinerary
      erb 'itinerary/providebasicinfo'.to_sym
    end
    
    post '/providebasicinfo' do
      @controller.itinerary.set_basic_info(params)
      redirect '/itinerary'
    end
         
    get '/save' do
      @controller.save_itinerary
      redirect '/'
    end
    get '/instantiate/:id' do 
      @itinerary = @controller.get_itinerary_by_id(params[:id])
      raise "L'itinerario non e' completo" unless @itinerary.status == :complete
      erb 'itinerary/instantiate'.to_sym
    end
    post '/instantiate/:id' do 
      itinerary = @controller.get_itinerary_by_id(params[:id])
      itinerary.instantiate(@creator.user, {:name => params[:name],:start => Date.parse(params[:start]),:end => Date.parse(params[:end])})
      redirect '/'
    end
  end 
end
