#Awesome Journeys

Software Engineering course project

### ISTRUZIONI per l'USO

*ATTENZIONE* Rifare la migration ogni volta che fate una pull (altrimenti se hanno aggiornato delle tabelle potrebbe
venire fuori un errore del server).

Per poter avviare il server e far funzionare tutto bisogna prima di tutto avere le gemme giuste installate.

Da terminale:

1. `bundle`  # questo significa che dovete avere la gemma bundler per dare questo comando.
    Se c'è un errore riguardante la gemma "do_mysql" vuol dire che prima dovete installarvi mysql.
    Se preferite invece potete usare sqlite semplicemente decommentando una linea apposita in "database/config.rb".
2. Eseguire `ruby database/migration.rb` per creare le tabelle nel db.
2.  Per avviare il server con sinatra `rackup config.ru`
3.  Enjoy.

### BUG NOTI

Si veda il repository su bitbucket nella sezione "issues".

### TODO

Implementare le conferme di cancellazione con javascript e popup.
