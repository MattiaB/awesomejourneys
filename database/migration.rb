require './awesome_journeys.rb'
DataMapper.auto_migrate!
#migration 1, :create_people_table do
#  up do
#    create_table :stay_templates do
#      column :id,   Integer, :serial => true
#    end
#  end
#  down do
#    drop_table :stay_templates
#  end
#end

# Populates the database
include AwesomeJourneys

# Users
customer = Customer.create!('customer', 'password')
travel_agent = TravelAgent.create!('travelagent', 'password')

# Locations
palermo = Location.create(:name => 'Palermo')
torino = Location.create(:name => 'Torino')
reggio_calabria = Location.create(:name => 'Reggio Calabria')
messina = Location.create(:name => 'Messina')
palermo.save!
torino.save!
reggio_calabria.save!
messina.save!

# Activity templates
activity_templates = []
activity_templates << ActivityTemplate.new(
  :name => 'Giornata in spiaggia',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 1.days.to_i,
  :cost => 8,
  :location => palermo
)
activity_templates << ActivityTemplate.new(
  :name => 'Cannolo party',
  :description => 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  :duration => 1.days.to_i,
  :cost => 5,
  :location => palermo
)
activity_templates << ActivityTemplate.new(
  :name => 'Catacombe',
  :description => 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
  :duration => 1.days.to_i,
  :cost => 4,
  :location => palermo
)
activity_templates << ActivityTemplate.new(
  :name => 'Museo Egizio',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 1.days.to_i,
  :cost => 10,
  :location => torino
)
activity_templates << ActivityTemplate.new(
  :name => 'Mole Antoneliana',
  :description => 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  :duration => 1,
  :cost => 12,
  :location => torino
)
activity_templates << ActivityTemplate.new(
  :name => 'Disco Night',
  :description => 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
  :duration => 1.days.to_i,
  :cost => 14,
  :location => torino
)
activity_templates.each do |activity_template| activity_template.save! end

# Stay Template Leaves
# Accomodations
hotel_rio = Accomodation.new(
  :name => 'Hotel Rio',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 3.days.to_i,
  :daily_cost => 40,
  :classification => 3,
  :start_location => torino,
  :end_location => torino
)
hotel_rio.save!

hotel_victoria = Accomodation.new(
  :name => 'Hotel Victoria',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 4.days.to_i,
  :daily_cost => 170,
  :classification => 5,
  :start_location => torino,
  :end_location => torino
)
hotel_victoria.save!

hotel_statuto = Accomodation.new(
  :name => 'Hotel Statuto',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 2.days.to_i,
  :daily_cost => 20,
  :classification => 2,
  :start_location => torino,
  :end_location => torino
)
hotel_statuto.save!

hotel_casciabanca = Accomodation.new(
  :name => 'Hotel Casciabanca',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 2.days.to_i,
  :daily_cost => 50,
  :classification => 3,
  :start_location => palermo,
  :end_location => palermo
)
hotel_casciabanca.save!

hotel_giufa = Accomodation.new(
  :name => 'Hotel Giufa',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 4.days.to_i,
  :daily_cost => 25,
  :classification => 2,
  :start_location => palermo,
  :end_location => palermo
)
hotel_giufa.save!

hotel_mimi = Accomodation.new(
  :name => 'Hotel Mimi',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 3.days.to_i,
  :daily_cost => 140,
  :classification => 5,
  :start_location => palermo,
  :end_location => palermo
)
hotel_giufa.save!
hotel_excelsior = Accomodation.new(
  :name => 'Grand Hotel Excelsior',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 7.days.to_i,
  :daily_cost => 240,
  :classification => 4,
  :start_location => reggio_calabria,
  :end_location => reggio_calabria
)
hotel_excelsior.save!

# Transports
pulman_torino = Transport.new(
  :name => 'Trasporto Urbano',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 1.days.to_i,
  :transport_type => :bus,
  :transport_cost => 2,
  :start_location => torino,
  :end_location => torino
)
pulman_torino.save!

treno_torino_reggio_calabria = Transport.new(
  :name => 'Treno Torino - Reggio Calabria',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 1.days.to_i,
  :transport_type => :train,
  :transport_cost => 124,
  :start_location => torino,
  :end_location => reggio_calabria
)
treno_torino_reggio_calabria.save!

traghetto_stretto_messina = Transport.new(
  :name => 'Traghetto Stretto di Messina',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 1.days.to_i,
  :transport_type => :ferry,
  :transport_cost => 20,
  :start_location => reggio_calabria,
  :end_location => messina
)
traghetto_stretto_messina.save!

pulman_messina_palermo = Transport.new(
  :name => 'Traghetto Stretto di Messina',
  :description => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  :duration => 1.days.to_i,
  :transport_type => :bus,
  :transport_cost => 7,
  :start_location => messina,
  :end_location => palermo
)
pulman_messina_palermo.save!