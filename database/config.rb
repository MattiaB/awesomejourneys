require 'data_mapper'

if ENV['RACK_ENV'] != 'production'
  #DataMapper::Logger.new(STDOUT, :debug)
end
# Commentare / Decommentare in base alle esigenze. Io consiglierei di usare SQLite
# visto che ora funge a meraviglia (l'ho testato piu volte prima di pushare) avendo
# cambiato solo una linea di codice nel setup.
# [Cristian]
DataMapper.setup(:default, ENV['DATABASE_URL'] || 'mysql://root:root@localhost/database') # Database MySQL
#DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/database/test.db") # Database SQLite
#DataMapper.auto_upgrade!