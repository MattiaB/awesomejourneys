require './test/test.rb'

class TestStayTemplate < Test::Unit::TestCase
  include AwesomeJourneys
  context "StayTemplate" do
    setup do
      location = Location.create(:name => "london")
      activity_template = ActivityTemplate.create(:name => "Madame Tussauds London", :description => "Madame Tussauds is a wax museum in London with branches in a number of major cities.",
      :duration => 2.hours.to_i, :cost => 30, :location => location)
      @activity = Activity.create(:activity_template => activity_template)
      @staytemplate = StayTemplate.new(:name=>"Londra",
                                          :description => "Attrazioni Londra",
                                          :duration => 1.day.to_i,
                                          :start_location => location,
                                          :end_location => location)
    end
    should "add activity" do
      assert @staytemplate.add_activity(@activity)
      assert @staytemplate.save
    end
    should "return right cost" do
      @staytemplate.add_activity(@activity)
      assert_equal(30.0,@staytemplate.cost)
    end

  end

end