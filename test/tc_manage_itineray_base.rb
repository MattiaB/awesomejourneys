require './test/test.rb'

class TestManageItinerayBase < Test::Unit::TestCase
  include AwesomeJourneys
  context "User Case Manage Itineray Base" do
    setup do
      Itinerary.all(:name => 'Palermo').each do |itinerary|
        itinerary.delete
      end
      @creator = Customer.create!("Test 1", "test").get_creator
      @controller = @creator.get_controller
    end

    should "create a complete itinerary" do
      itinerary = @controller.create_itinerary
      assert itinerary.kind_of? Itinerary
      assert_equal @creator.user, itinerary.user
      stay_template = Accomodation.first(:name => 'Hotel Casciabanca')

      @controller.set_basic_info(:name => 'Palermo',:description => 'Test', :category => 'viaggio,sicilia')
      assert_equal 'Palermo', itinerary.name
      assert_equal 'Test', itinerary.description
      assert_equal 'viaggio,sicilia', itinerary.category

      current_stay = @controller.select_stay(stay_template.id)
      assert_equal stay_template, current_stay.stay_template

      activity_template = ActivityTemplate.first(:name => 'Giornata in spiaggia')
      @controller.add_activity(activity_template.id)
      assert_equal activity_template, @controller.get_activities.first.activity_template

      @controller.save_stay
      #Problema con la save_stay non salva la stay nel db perchè non esiste itinerary nel db
      assert_not_nil itinerary.stays.first.id
      assert_not_nil @controller.current_stay.id

      @controller.save_itinerary
      assert_not_nil itinerary.id
      assert_not_nil itinerary.stays.first.id
      assert_not_nil @controller.current_stay.id
      itinerary_id = itinerary.id
      @controller.delete_itinerary(itinerary.id)
      assert Itinerary.get(itinerary_id).nil?
    end

    def create_basic_itinerary
      itinerary = @controller.create_itinerary
      @controller.set_basic_info(:name => 'Palermo',:description => 'Test', :category => 'viaggio,sicilia')
      stay_template = Accomodation.first(:name => 'Hotel Casciabanca')
      current_stay = @controller.select_stay(stay_template.id)
      activity_template = ActivityTemplate.first(:name => 'Giornata in spiaggia')
      @controller.add_activity(activity_template.id)
      yield(current_stay, activity_template)
      @controller.save_itinerary
      @controller.delete_itinerary(itinerary.id)
    end
    should "delete a stay in itinerary" do
      create_basic_itinerary do |current_stay|
        @controller.itinerary
        itinerary = @controller.save_stay
        assert itinerary.stays.count == 1, itinerary.stays.inspect
        @controller.delete_stay(current_stay.id)
        assert itinerary.stays.count == 0
      end
    end

    should "delete a activity in a stay" do
      create_basic_itinerary do |current_stay|
        activity = @controller.current_stay.activities.first
        @controller.delete_activity(activity.id)
        assert @controller.current_stay.activities.count == 0

        @controller.save_stay
        @controller.delete_stay(current_stay.id)
      end
    end

    should 'find and add a transfer stay' do
      itinerary = @controller.create_itinerary
      @controller.set_basic_info(:name => 'Torino to Reggio Calabia',:description => 'Test', :category => 'viaggio')
      stay_template = Accomodation.first(:name => 'Hotel Victoria')
      torino_stay = @controller.select_stay(stay_template.id)
      @controller.save_stay

      assert itinerary.stays.count == 1
      assert itinerary.stays.first.offset_number == 0
      stay_template = Accomodation.first(:name => 'Grand Hotel Excelsior')
      @controller.select_stay(stay_template.id)
      @controller.save_stay
      
      assert itinerary.stays.last.offset_number == 1
      assert itinerary.stays.count == 2
      # Voglio inserire una tappa di trasferimento

      @controller.add_transfer_stay(torino_stay.id)

      assert itinerary.stays.count == 3
      assert itinerary.stays.length == 3
      assert itinerary.stays[1].stay_template.kind_of?(Transport), itinerary.stays.inspect
      assert itinerary.stays[1].start_location.name == 'Torino'
      assert itinerary.stays[1].end_location.name == 'Reggio Calabria'
      assert itinerary.stays[1].offset_number == 1
      assert itinerary.stays[2].offset_number == 2
      
      @controller.save_itinerary
      @controller.delete_itinerary(itinerary.id)
    end
  end

end