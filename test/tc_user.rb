require './test/test.rb'

class TestUser < Test::Unit::TestCase
  include AwesomeJourneys
  context "User" do
    setup do
      User.all(:name.like => 'CustomerTest').destroy
      password_salt = BCrypt::Engine.generate_salt
      password_hash = BCrypt::Engine.hash_secret("test", password_salt)
      Customer.create(:name => "CustomerTest", :salt => password_salt, :passwordhash => password_hash)
    end
    should "login" do
      user = User.login("CustomerTest","test")
      assert_not_nil user
      user = User.login("NoFound","test")
      assert user.nil?
    end
    should "create" do
      assert !User.create!('CustomerTest create', 'test').nil?
      assert_raise RuntimeError do
        User.create!('', '')
      end
    end
    should "not create two user with same username'" do
      assert User.create!('CustomerTest same username', 'test')
      assert User.create!('CustomerTest same username', 'test')
      assert User.count(:name => 'CustomerTest same username') == 1
    end
  end
  context "DecoratedUser" do
    setup do
      @customer     =     Customer.first_or_create({:name => 'Test Customer 1'},{:passwordhash => 'test', :salt=>  'test'})
      @travel_agent =   TravelAgent.first_or_create({:name => 'Test Travel Agent 1'},{:passwordhash => 'test', :salt=>  'test'})
    end
    should "have same methods of User" do
      customer_creator = CustomerCreator.new @customer
      assert_respond_to(customer_creator, :name)
      assert_respond_to(customer_creator, :itineraries)
      assert_respond_to(customer_creator, :journeys)
    end
    should "have only one decorator and only User can be decorated" do
      customer_creator = CustomerCreator.new @customer
      assert_raise RuntimeError do
        CustomerCreator.new customer_creator
      end
    end
    should "if is Customer, don't to be a Travel agent Creator'" do
      assert_raise RuntimeError do
        TravelAgentCreator.new @customer
      end
    end
    should "return self creator" do
      customer_creator = @customer.get_creator
      assert customer_creator.kind_of? CustomerCreator
      traverl_agent_creator = @travel_agent.get_creator
      assert traverl_agent_creator.kind_of? TravelAgentCreator
    end
    should "return ritgh controller" do
      customer_creator = CustomerCreator.new @customer
      travel_agent_creator = TravelAgentCreator.new @travel_agent
      assert customer_creator.get_controller.kind_of? ManagementController
      assert travel_agent_creator.get_controller.kind_of? ManagementControllerPro
    end
  end

end