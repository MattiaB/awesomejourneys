require './test/test.rb'

class TestSearch < Test::Unit::TestCase
  include AwesomeJourneys
  context 'Search Itinerary Or Journey' do
    setup do
      @user = User.create!('Test user 1', 'test')
      @it1 = Itinerary.first_or_create(:name => 'Viaggio a Roma', :description => 'Visita ai musei vaticani',:category=> 'musei,chiesa,vaticano', :user=> @user)
      @jo1 = Journey.first_or_create(:user => @user, :itinerary => @it1, :name=> 'Il mio viaggio a Roma')
    end
    should "search some itineraries and journery" do
      controller = GraspSearchController.new(@user)
      results = controller.search_itinerary_or_journey('roma')
      assert results.kind_of? SearchResultsItineraryOrJourney
      assert_equal results.user, @user
      assert_equal results.itineraries.first, @it1
      assert_equal results.journeys.first, @jo1
    end
  end
  context 'Search Stay' do
    setup do
      @user = User.create!('Test user 1', 'test')
     # @stay_templates = StayTemplate.all(:name => 'Hotel')
    end
    should "search some staytemplate" do
      controller = GraspSearchController.new(@user)
      results = controller.search_stay('Hotel')
      assert results.kind_of? SearchResultsStay
      assert_equal results.user, @user
      #assert_equal results.stay_templates, @stay_templates
    end
  end
  context 'Search Activity' do
    setup do
      @user = User.create!('Test user 1', 'test')
     #@activity_template

    end
    should "search some itineraries and journery" do
      controller = GraspSearchController.new(@user)
      results = controller.search_activities()
      assert results.kind_of? SearchResultsActivity
      assert_equal results.user, @user
      #assert_equal results.activity_templates.first, @activity_template
    end
  end
end